# -*- coding: utf-8 -*-

from readex import readex
from readex.readex import Readex

import unittest


class TestSingleRegex(unittest.TestCase):
    """Test cases with a single regex in a Readex object."""

    def test_end(self):
        r = Readex()
        r.end('foo')
        self.assertEqual(r.regex, 'foo$')

    def test_end_before_newline(self):
        r = Readex()
        r.end('foo', before_newline=False)
        self.assertEqual(r.regex, 'foo\A')

    def test_word(self):
        r = Readex()
        r.word()
        self.assertEqual(r.regex, '\w')

    def test_non_word(self):
        r = Readex()
        r.non_word()
        self.assertEqual(r.regex, '\W')

    def test_whitespace(self):
        r = Readex()
        r.whitespace()
        self.assertEqual(r.regex, '\s')

    def test_non_whitespace(self):
        r = Readex()
        r.non_whitespace()
        self.assertEqual(r.regex, '\S')

    def test_digit(self):
        r = Readex()
        r.digit()
        self.assertEqual(r.regex, '\d')

    def test_non_digit(self):
        r = Readex()
        r.non_digit()
        self.assertEqual(r.regex, '\D')

    def test_word_boundary(self):
        r = Readex()
        r.word_boundary()
        self.assertEqual(r.regex, '\b')

    def test_non_word_boundary(self):
        r = Readex()
        r.non_word_boundary()
        self.assertEqual(r.regex, '\B')

    def test_start(self):
        r = Readex()
        r.start('foo')
        self.assertEqual(r.regex, '\Afoo')

    def test_start_multiline(self):
        r = Readex(multiline=True)
        r.start('foo')
        self.assertEqual(r.regex, '^foo')

    def test_comment(self):
        r = Readex()
        r.comment('comment please')
        self.assertEqual(r.regex, '(?#comment please)')

    def test_capturing_group(self):
        r = Readex()
        r.group(Readex().digit())
        self.assertEqual(r.regex, '(\d)')

    def test_non_capturing_group(self):
        r = Readex()
        r.group(Readex().digit(), capture=False)
        self.assertEqual(r.regex, '(?:\d)')

    def test_previous_group(self):
        r = Readex()
        r.previous_group('foo')
        self.assertEqual(r.regex, '(?=foo)')

    def test_alternate(self):
        r = Readex()
        r.alternate(readex.DIGIT, 'abc', readex.DIGIT)
        self.assertEqual(r.regex, '\d|abc|\d')

    def test_followed_by(self):
        r = Readex()
        r.followed_by(readex.DIGIT)
        self.assertEqual(r.regex, '(?=\d)')

    def test_not_followed_by(self):
        r = Readex()
        r.not_followed_by(readex.WHITESPACE)
        self.assertEqual(r.regex, '(?!\s)')

    def test_follows(self):
        r = Readex()
        r.follows(readex.NON_WORD_BOUNDARY)
        self.assertEqual(r.regex, '(?<=\B)')

    def test_not_follows(self):
        r = Readex()
        r.not_follows(readex.WORD)
        self.assertEqual(r.regex, '(?!\w)')

    def test_previous_group_by_name(self):
        r = Readex()
        r.previous_group('foo')
        self.assertEqual(r.regex, '(?=foo)')

    def test_previous_group_by_number(self):
        r = Readex()
        r.previous_group(2)
        self.assertEqual(r.regex, r'\2')