# -*- coding: utf-8 -*-

import re

DIGIT = r'\d'
NON_DIGIT = r'\D'

WORD = r'\w'
NON_WORD = r'\W'
WORD_BOUNDARY = r'\b'
NON_WORD_BOUNDARY = r'\B'

WHITESPACE = r'\s'
NON_WHITESPACE = r'\S'


class Readex(object):
    def __init__(self, multiline=False, locale=None, unicode=None):
        self.multiline = multiline
        self.locale = locale
        self.unicode = unicode
        self._regex = []

    @property
    def regex(self):
        return r''.join(self._regex)

    def literal(self, literal):
        """Adds literal regex containing one or more characters."""
        self._regex.append(literal)
        return self

    def start(self, expr):
        """Adds regex ``^`` or ``\A``.

        If in multiline mode, adds regex ``^`` which matches at the start of a
        string and also immediately after each newline.
        If not in multiline mode, add regex ``\A`` which matches only at the
        start of a string.

        :param expr: expression at the start of a string.
        """
        self._regex.append('^') if self.multiline else self._regex.append('\A')
        self._regex.append(expr)
        return self

    def end(self, expr, before_newline=True):
        """Adds regex to match the end of a string.

        :param expr: expression at the end of a string.
        :param before_newline: if ``True``, adds regex to match the end of a
            string or just before the newline at the end of a string.
        """
        self._regex.append(expr)
        self._regex.append('$') if before_newline else self._regex.append('\A')
        return self

    def word_boundary(self):
        """Adds regex ``\b``."""
        self._regex.append(WORD_BOUNDARY)
        return self

    def non_word_boundary(self):
        """Adds regex ``\B``."""
        self._regex.append(NON_WORD_BOUNDARY)
        return self

    def digit(self):
        """Adds regex ``\d``."""
        self._regex.append(DIGIT)
        return self

    def non_digit(self):
        """Adds regex ``\D``."""
        self._regex.append(NON_DIGIT)
        return self

    def whitespace(self):
        """Adds regex ``\s``"""
        self._regex.append(WHITESPACE)
        return self

    def non_whitespace(self):
        """Adds regex ``\S``."""
        self._regex.append(NON_WHITESPACE)
        return self

    def word(self):
        """Adds regex ``\w``."""
        self._regex.append(WORD)
        return self

    def non_word(self):
        """Add regex ``\W``."""
        self._regex.append(NON_WORD)
        return self

    def alternate(self, *regexes):
        """Adds regex ``...|...|..."""
        regex = '|'.join([self._from_object(readex) for readex in regexes])
        self._regex.append(regex)
        return self

    def group(self, readex, capture=True):
        """Adds regex ``(...)``.

        :param capture: if ``False``, adds regex (?:...)
        """
        regex = '({0})' if capture else '(?:{0})'
        self._regex.append(regex.format(readex.regex))
        return self

    def group_by_name(self, readex, name):
        """Adds regex ``(?P<name>...)``."""
        self._regex.append('(?P<{0}>{1})'.format(name, readex.regex))
        return self

    def previous_group(self, group):
        """Adds regex ``(?=name)`` or ``\number``."""
        regex = '(?={0})' if isinstance(group, str) else '\{0}'
        self._regex.append(regex.format(group))
        return self

    def comment(self, comment):
        """Adds regex ``(?#...)``."""
        self._regex.append('(?#{0})'.format(comment))
        return self

    def followed_by(self, regex):
        """Adds regex ``(?=...)``."""
        self._regex.append('(?={0})'.format(self._from_object(regex)))
        return self

    def not_followed_by(self, regex):
        """Adds regex ``(?!...)``."""
        self._regex.append('(?!{0})'.format(self._from_object(regex)))
        return self

    def follows(self, regex):
        """Adds regex ``(?<=...)``."""
        self._regex.append('(?<={0})'.format(self._from_object(regex)))
        return self

    def not_follows(self, regex):
        """Adds regex ``(?!...)``."""
        self._regex.append('(?!{0})'.format(self._from_object(regex)))
        return self

    def conditional(self, condition, yes_pattern, no_pattern=None):
        """Adds regex ``(?(id/name)yes-pattern|no-pattern)``.

        :param condition: condition can be a group name, group id or any regex
        :param yes_pattern: if the group exists or the condition matches,
            matches ``yes_pattern``.
        :param no_pattern: if supplied, matches ``no_pattern`` if the condition
            if fails.
        """
        condition = self._from_object(condition)
        yes_pattern = self._from_object(yes_pattern)
        if no_pattern:
            regex = '(?({0}){1})|{2})'.format(condition, yes_pattern,
                                              self._from_object(no_pattern))
        else:
            regex = '(?({0}){1})'.format(condition, yes_pattern)
        self._regex.append(regex)
        return self

    def findall(self, text):
        return re.findall(self.regex, text)

    @staticmethod
    def _from_object(obj):
        """Retrieves the regex from the input object.

        The input object can be a String or a Readex object.
        """
        return obj.regex if isinstance(obj, Readex) else obj